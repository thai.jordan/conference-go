# The default() method of JSONEncoder subclasses is called only when the encoder 
# encounters an object it doesn't otherwise know how to serialize.
# Python lists translate to JSON arrays. What it is giving you is a 
# perfectly valid JSON string that could be used in a Javascript application. 
# To get what you expected, you would need to use a dict:


from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_weather_data, get_photo

# ENCODERS

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]





# USE DECORATOR HERE, MAKE SURE TO IMPORT!!!
# LIST VIEWS
@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


# @require_http_methods(["DELETE", "GET", "PUT"])
# def api_show_conference(request, id):
#     if request.method == "GET":
#         conference = Conference.objects.get(id=id)
#         return JsonResponse(
#             conference,
#             encoder=ConferenceDetailEncoder,
#             safe=False
#         )
#     elif request.method == "DELETE":
#         count, _ = Conference.objects.filter(id=id).delete()
#         return JsonResponse({"deleted": count > 0})
#     else:
#         content = json.loads(request.body)
#         try:
#             if "conference" in content:
#                 conference = Conference.objects.get(["conference"])
#                 content["conference"] = conference
#         except Conference.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid conference id"},
#                 status=400,
#             )
#         Location.objects.filter(id=id).update(**content)

#         location = Location.objects.get(id=id)
#         return JsonResponse(
#             location,
#             encoder=LocationDetailEncoder,
#             safe=False,
#         )

# DETAIL VIEW import methods
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)

        city = conference.location.city
        state = conference.location.state.abbreviation
        weather = get_weather_data(city, state)

        return JsonResponse(
            {"weather": weather, "conference": conference},
            encoder=ConferenceDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)

        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


# @require_http_methods(["GET", "POST"])
# def api_list_locations(request):
#     if request.method == "GET":
#         locations = Location.objects.all()
#         return JsonResponse(
#             {"locations": locations},
#             encoder=LocationListEncoder,
#         )
#     else:
#         content = json.loads(request.body)
#         try:
#             state = State.objects.get(abbreviation=content["state"])
#             content["state"] = state
#         except State.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid state abbreviation"},
#                 status=400,
#             )

#         location = Location.objects.create(**content)
#         return JsonResponse(
#             location,
#             encoder=LocationDetailEncoder,
#             safe=False,
#         )

# LIST VIEWS IMPORT METHODS
@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            # print("Content", content)
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
            photo = get_photo(content["city"], content["state"].abbreviation)
            content.update(photo)
            location = Location.objects.create(**content)
            return JsonResponse(
                location,
                encoder=LocationDetailEncoder,
                safe=False,
            )
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

# DETAIL VIEW . IMPORT METHODS
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
    # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        Location.objects.filter(id=id).update(**content)

        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )