# acl is a seperate file where we make 3rd party requests and fit the data
# we receive to our model. because it makes our life easier :)

# importing api keys from keys.py
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
        }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # grab url from docs and input its requirements insomnia. same goes above url
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"]
        }
    except (KeyError, IndexError):
        return None


def get_photo(city, state):
    # put info on header tab in insomnia
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    # grab url from docs and input its requirements insomnia
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}